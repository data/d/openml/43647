# OpenML dataset: Indian-News-Articles

https://www.openml.org/d/43647

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
It's always interesting to analyse what's going on in the news but there's no good data avaialable in indian context on kaggle, so I created one.
Content
Apart from the main content of the articles, each row contains author, link, publish date etc.
Acknowledgements
Grateful to the creator of python, beautifulsoup library and firstpost.com.
Inspiration
Data is not always available to us, sometimes we need to create one, this is my attempt to do that.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43647) of an [OpenML dataset](https://www.openml.org/d/43647). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43647/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43647/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43647/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

